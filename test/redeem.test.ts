const { expect, assert } = require("chai");
import { ethers, network } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { contracts, Bridge, ERC20BridgableToken } from "../src/types";
import { BigNumber, Signature } from "ethers";

const NAME: string = "IVKLIM";
const SYMBOL: string = "KLIM";
const MINT_AMOUNT = 10000;

describe("Redeem ", function () {
  let tokenFrom: ERC20BridgableToken;
  let tokenTo: ERC20BridgableToken;
  let crossChainBridge: Bridge;
  let accounts: SignerWithAddress[];
  let chainIdFrom: number;
  let chainIdTo: number;
  let validator: SignerWithAddress;
  let sender: SignerWithAddress;
  let nonce = 1;
  let sig: Signature;
  let msg: string;
  let msgBytes: Uint8Array;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chainIdFrom = network.config.chainId as number;
    chainIdTo = chainIdFrom;
    validator = accounts[5];

    //first token
    const factory = await ethers.getContractFactory("ERC20BridgableToken");
    tokenTo = await factory.deploy(NAME, SYMBOL);
    await tokenTo.deployed();
    //second token
    const factory2 = await ethers.getContractFactory("ERC20BridgableToken");
    tokenFrom = await factory2.deploy(NAME, SYMBOL);
    await tokenFrom.deployed();

    const factoryBridge = await ethers.getContractFactory("Bridge");
    crossChainBridge = await factoryBridge.deploy(validator.address);
    await crossChainBridge.deployed();

    crossChainBridge.setBridgableTokens(
      chainIdTo,
      tokenFrom.address,
      tokenTo.address
    );

    sender = accounts[1];
    await tokenFrom.mint(sender.address, MINT_AMOUNT);
    await tokenFrom
      .connect(sender)
      .approve(crossChainBridge.address, MINT_AMOUNT);

    // grant MINTER_ROLE for token2
    await tokenTo.grantRole(
      await tokenTo.MINTER_ROLE(),
      crossChainBridge.address
    );

    //on validator side
    msg = ethers.utils.solidityKeccak256(
      [
        "uint256",
        "uint256",
        "uint256",
        "address",
        "address",
        "uint128",
        "address",
      ],
      [
        nonce,
        chainIdFrom,
        chainIdTo,
        tokenFrom.address,
        tokenTo.address,
        MINT_AMOUNT,
        sender.address,
      ]
    );
    msgBytes = ethers.utils.arrayify(msg);
    let signature = await validator.signMessage(msgBytes);
    sig = await ethers.utils.splitSignature(signature);
  });

  it("After redeem will mint tokens", async function () {
    const amountTokensBeforeSwap = await tokenTo.balanceOf(sender.address);

    await crossChainBridge
      .connect(sender)
      .redeem(
        nonce,
        chainIdFrom,
        chainIdTo,
        tokenFrom.address,
        tokenTo.address,
        MINT_AMOUNT,
        sender.address,
        sig.v,
        sig.r,
        sig.s
      );

    const amountTokensAfterSwap = await tokenTo.balanceOf(sender.address);

    expect((await amountTokensAfterSwap).sub(amountTokensBeforeSwap)).to.equal(
      MINT_AMOUNT
    );
  });

  it("After swap will mint and totalSupply decrease ", async function () {
    const totalSupplyBeforeSwap = await tokenTo.totalSupply();

    await crossChainBridge
      .connect(sender)
      .redeem(
        nonce,
        chainIdFrom,
        chainIdTo,
        tokenFrom.address,
        tokenTo.address,
        MINT_AMOUNT,
        sender.address,
        sig.v,
        sig.r,
        sig.s
      );

    const totalSupplyAfterSwap = await tokenTo.balanceOf(sender.address);

    expect((await totalSupplyAfterSwap).sub(totalSupplyBeforeSwap)).to.equal(
      MINT_AMOUNT
    );
  });

  describe("Reverted", function () {
    it("incorrect signer", async () => {
      let signature2 = await accounts[1].signMessage(msgBytes);
      const sig2 = await ethers.utils.splitSignature(signature2);

      await expect(
        crossChainBridge
          .connect(sender)
          .redeem(
            nonce,
            chainIdFrom,
            chainIdTo,
            tokenFrom.address,
            tokenTo.address,
            MINT_AMOUNT,
            sender.address,
            sig2.v,
            sig2.r,
            sig2.s
          )
      ).to.be.revertedWith("Bridge: incorrect signer");
    });

    it("redeem executed", async () => {
      await crossChainBridge
        .connect(sender)
        .redeem(
          nonce,
          chainIdFrom,
          chainIdTo,
          tokenFrom.address,
          tokenTo.address,
          MINT_AMOUNT,
          sender.address,
          sig.v,
          sig.r,
          sig.s
        );

      await expect(
        crossChainBridge
          .connect(sender)
          .redeem(
            nonce,
            chainIdFrom,
            chainIdTo,
            tokenFrom.address,
            tokenTo.address,
            MINT_AMOUNT,
            sender.address,
            sig.v,
            sig.r,
            sig.s
          )
      ).to.be.revertedWith("Bridge: redeem executed");
    });
  });
});
