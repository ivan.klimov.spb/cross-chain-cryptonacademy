const { expect, assert } = require("chai");
import { ethers, network } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { contracts, Bridge, ERC20BridgableToken } from "../src/types";
import { BigNumber } from "ethers";

const NAME: string = "IVKLIM";
const SYMBOL: string = "KLIM";
const MINT_AMOUNT = 10000;

describe("Check sign ", function () {
  let tokenFrom: ERC20BridgableToken;
  let tokenTo: ERC20BridgableToken;
  let crossChainBridge: Bridge;
  let accounts: SignerWithAddress[];
  let chainIdFrom: number;
  let chainIdTo: number;
  let validator: SignerWithAddress;
  let sender: SignerWithAddress;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chainIdFrom = network.config.chainId as number;
    chainIdTo = chainIdFrom;
    validator = accounts[5];

    //first token
    const factory = await ethers.getContractFactory("ERC20BridgableToken");
    tokenTo = await factory.deploy(NAME, SYMBOL);
    await tokenTo.deployed();
    //second token
    const factory2 = await ethers.getContractFactory("ERC20BridgableToken");
    tokenFrom = await factory2.deploy(NAME, SYMBOL);
    await tokenFrom.deployed();

    const factoryBridge = await ethers.getContractFactory("Bridge");
    crossChainBridge = await factoryBridge.deploy(validator.address);
    await crossChainBridge.deployed();

    crossChainBridge.setBridgableTokens(
      chainIdTo,
      tokenFrom.address,
      tokenTo.address
    );

    sender = accounts[1];
    await tokenFrom.mint(sender.address, MINT_AMOUNT);
    await tokenFrom
      .connect(sender)
      .approve(crossChainBridge.address, MINT_AMOUNT);
  });

  it("Should be checked with the same params", async function () {
    let nonce = 1;
    let receiver = accounts[1].address;

    let msg = ethers.utils.solidityKeccak256(
      [
        "uint256",
        "uint256",
        "uint256",
        "address",
        "address",
        "uint128",
        "address",
      ],
      [
        nonce,
        chainIdFrom,
        chainIdTo,
        tokenFrom.address,
        tokenTo.address,
        MINT_AMOUNT,
        receiver,
      ]
    );

    let msgBytes = ethers.utils.arrayify(msg);

    let signature = await validator.signMessage(msgBytes);

    let sig = await ethers.utils.splitSignature(signature);

    let signed = await crossChainBridge
      .connect(receiver)
      .checkSign(
        nonce,
        chainIdFrom,
        chainIdTo,
        tokenFrom.address,
        tokenTo.address,
        MINT_AMOUNT,
        receiver,
        sig.v,
        sig.r,
        sig.s
      );

    expect(signed).to.be.true;
  });

  it("Should be not valid", async function () {
    let nonce = 1;
    let receiver = accounts[1].address;

    let msg = ethers.utils.solidityKeccak256(
      [
        "uint256",
        "uint256",
        "uint256",
        "address",
        "address",
        "uint128",
        "address",
      ],
      [
        nonce,
        chainIdFrom,
        chainIdTo,
        tokenFrom.address,
        tokenTo.address,
        MINT_AMOUNT,
        receiver,
      ]
    );

    let msgBytes = ethers.utils.arrayify(msg);

    let signature = await validator.signMessage(msgBytes);

    let sig = await ethers.utils.splitSignature(signature);

    let signed = await crossChainBridge
      .connect(receiver)
      .checkSign(
        nonce,
        chainIdFrom,
        chainIdTo,
        tokenFrom.address,
        tokenTo.address,
        MINT_AMOUNT,
        accounts[2].address,
        sig.v,
        sig.r,
        sig.s
      );

    expect(signed).to.be.false;
  });

  it("Should be not valid if not validator sign", async function () {
    let nonce = 1;
    let receiver = accounts[1].address;

    let msg = ethers.utils.solidityKeccak256(
      [
        "uint256",
        "uint256",
        "uint256",
        "address",
        "address",
        "uint128",
        "address",
      ],
      [
        nonce,
        chainIdFrom,
        chainIdTo,
        tokenFrom.address,
        tokenTo.address,
        MINT_AMOUNT,
        receiver,
      ]
    );

    let msgBytes = ethers.utils.arrayify(msg);

    let signature = await accounts[1].signMessage(msgBytes);

    let sig = await ethers.utils.splitSignature(signature);

    let signed = await crossChainBridge
      .connect(receiver)
      .checkSign(
        nonce,
        chainIdFrom,
        chainIdTo,
        tokenFrom.address,
        tokenTo.address,
        MINT_AMOUNT,
        receiver,
        sig.v,
        sig.r,
        sig.s
      );

    expect(signed).to.be.false;
  });
});
