const { expect, assert } = require("chai");
import { ethers, network } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { contracts, Bridge, ERC20BridgableToken } from "../src/types";
import { BigNumber } from "ethers";

const NAME: string = "IVKLIM";
const SYMBOL: string = "KLIM";
const MINT_AMOUNT = 10000;

describe("Swap ", function () {
  let tokenTo: ERC20BridgableToken;
  let tokenFrom: ERC20BridgableToken;
  let crossChainBridge: Bridge;
  let accounts: SignerWithAddress[];
  let chainIdTo: number;
  let chainIdFrom: number;
  let validator: SignerWithAddress;
  let sender: SignerWithAddress;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chainIdFrom = network.config.chainId as number;
    chainIdTo = chainIdFrom;
    validator = accounts[5];

    //first token
    const factory = await ethers.getContractFactory("ERC20BridgableToken");
    tokenTo = await factory.deploy(NAME, SYMBOL);
    await tokenTo.deployed();
    //second token
    const factory2 = await ethers.getContractFactory("ERC20BridgableToken");
    tokenFrom = await factory2.deploy(NAME, SYMBOL);
    await tokenFrom.deployed();

    const factoryBridge = await ethers.getContractFactory(
      "Bridge",
      accounts[0]
    );
    crossChainBridge = await factoryBridge.deploy(validator.address);
    await crossChainBridge.deployed();

    sender = accounts[1];
    await tokenFrom.mint(sender.address, MINT_AMOUNT);
    await tokenFrom
      .connect(sender)
      .approve(crossChainBridge.address, MINT_AMOUNT);
  });

  describe("Swap", function () {
    it("After swap will burn tokens and balance will change", async function () {
      const amountTokensBeforeSwap = await tokenFrom.balanceOf(sender.address);

      crossChainBridge.setBridgableTokens(
        chainIdTo,
        tokenFrom.address,
        tokenTo.address
      );

      await crossChainBridge
        .connect(sender)
        .swap(chainIdTo, tokenFrom.address, tokenTo.address, MINT_AMOUNT);

      const amountTokensAfterSwap = await tokenFrom.balanceOf(sender.address);

      expect(
        (await amountTokensAfterSwap).sub(amountTokensBeforeSwap)
      ).to.equal(-MINT_AMOUNT);
    });

    it("After swap will burn and totalSupply decrease ", async function () {
      const totalSupplyBeforeSwap = await tokenFrom.totalSupply();

      crossChainBridge.setBridgableTokens(
        chainIdTo,
        tokenFrom.address,
        tokenTo.address
      );

      await crossChainBridge
        .connect(sender)
        .swap(chainIdTo, tokenFrom.address, tokenTo.address, MINT_AMOUNT);

      const totalSupplyAfterSwap = await tokenFrom.balanceOf(sender.address);

      expect((await totalSupplyAfterSwap).sub(totalSupplyBeforeSwap)).to.equal(
        -MINT_AMOUNT
      );
    });

    describe("Emit", function () {
      it("Emit SwapExecuted(receiver, MINT_AMOUNT)", async () => {
        crossChainBridge.setBridgableTokens(
          chainIdTo,
          tokenFrom.address,
          tokenTo.address
        );

        await expect(
          crossChainBridge
            .connect(sender)
            .swap(chainIdTo, tokenFrom.address, tokenTo.address, MINT_AMOUNT)
        )
          .to.emit(crossChainBridge, "SwapInitialized")
          .withArgs(
            chainIdFrom,
            chainIdTo,
            tokenFrom.address,
            tokenTo.address,
            MINT_AMOUNT,
            sender.address
          );
      });
    });

    describe("Reverted", function () {
      it("not enough on the balance", async () => {
        await expect(
          crossChainBridge
            .connect(sender)
            .swap(chainIdTo, tokenFrom.address, tokenTo.address, MINT_AMOUNT)
        ).to.be.revertedWith("Bridge: no swappable pair");
      });
    });
  });
});
