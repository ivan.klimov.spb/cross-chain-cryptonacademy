const { expect, assert } = require("chai");
import { ethers, network } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { contracts, Bridge, ERC20BridgableToken } from "../src/types";
import { BigNumber } from "ethers";

const NAME: string = "IVKLIM";
const SYMBOL: string = "KLIM";
const MINT_AMOUNT = 10000;

describe("Set Function ", function () {
  let tokenTo: ERC20BridgableToken;
  let tokenFrom: ERC20BridgableToken;
  let crossChainBridge: Bridge;
  let accounts: SignerWithAddress[];
  let chainIdTo: number;
  let chainIdFrom: number;
  let validator: SignerWithAddress;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chainIdFrom = network.config.chainId as number;
    chainIdTo = chainIdFrom;
    validator = accounts[5];

    //first token
    const factory = await ethers.getContractFactory("ERC20BridgableToken");
    tokenTo = await factory.deploy(NAME, SYMBOL);
    await tokenTo.deployed();
    //second token
    const factory2 = await ethers.getContractFactory("ERC20BridgableToken");
    tokenFrom = await factory2.deploy(NAME, SYMBOL);
    await tokenFrom.deployed();

    const factoryBridge = await ethers.getContractFactory("Bridge");
    crossChainBridge = await factoryBridge.deploy(validator.address);
    await crossChainBridge.deployed();
  });

  it("Validator should be updated", async function () {
    crossChainBridge.setValidator(accounts[6].address);
    expect(await crossChainBridge.validator()).to.equal(accounts[6].address);
  });

  it("bridgableTokens should be updated", async function () {
    crossChainBridge.setBridgableTokens(
      chainIdTo,
      tokenFrom.address,
      tokenTo.address
    );
    expect(
      await crossChainBridge.getBridgableTokens(chainIdTo, tokenFrom.address)
    ).to.equal(tokenTo.address);
  });

  it("bridgableTokens eq zerro address before init", async function () {
    expect(
      await crossChainBridge.getBridgableTokens(chainIdTo, tokenFrom.address)
    ).to.equal(ethers.constants.AddressZero);
  });

  describe("Reverted", function () {
    it("Init address cant be zero", async () => {
      const factoryBridge = await ethers.getContractFactory("Bridge");
      await expect(
        crossChainBridge.setValidator(ethers.constants.AddressZero)
      ).to.be.revertedWith("Bridge: address can't be zero");
    });
  });
});
