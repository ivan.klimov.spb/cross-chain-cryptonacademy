const { expect, assert } = require("chai");
import { ethers, network } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { contracts, Bridge, ERC20BridgableToken } from "../src/types";
import { BigNumber } from "ethers";

describe("Deployment ", function () {
  let crossChainBridge: Bridge;
  let accounts: SignerWithAddress[];
  let chainId: number;
  let validator: SignerWithAddress;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chainId = network.config.chainId as number;
    validator = accounts[5];
  });

  describe("Deployment", function () {
    it("Should set the right validator", async function () {
      const factoryBridge = await ethers.getContractFactory("Bridge");
      crossChainBridge = await factoryBridge.deploy(validator.address);
      await crossChainBridge.deployed();

      expect(await crossChainBridge.validator()).to.equal(validator.address);
    });
  });
});
