// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

import "./IBridge.sol";
import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import "./IERC20MintableBurnable.sol";

contract Bridge is IBridge {
    using ECDSA for bytes32;

    address public validator;

    //to chain id => (from ERC20 => toERC20)
    mapping(uint256 => mapping(address => address)) _bridgableTokens;
    mapping(bytes32 => bool) _redeemExecuted;

    constructor (address _validator) {
        validator = _validator;
    }

    modifier addressCantBeZero(address erc20TokenAddress) {
        require(erc20TokenAddress != address(0), "Bridge: address can't be zero");
        _;
    }

    function checkSign(
        uint256 nonce,
        uint256 fromChainId,
        uint256 toChainId,
        address fromERC20,
        address toERC20,
        uint128 amount,
        address receiver,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external view override returns (bool) {
        bytes32 message  = _getMessage(nonce, fromChainId, toChainId, fromERC20, toERC20, amount, receiver);
        return _checkSign(message, v, r, s);
    }

    function getBridgableTokens(uint256 toChainId, address fromERC20) external view returns(address) {
        return _bridgableTokens[toChainId][fromERC20];
    }

    function setValidator(address newValidator) external addressCantBeZero(newValidator) {
        validator = newValidator;

        emit ValidatorUpdated(validator);
    }

    function setBridgableTokens(uint256 toChainId, address fromERC20, address toERC20) external {
        _bridgableTokens[toChainId][fromERC20] = toERC20;

        emit BridgableTokensUpdated(toChainId, fromERC20, toERC20);
    }

    function swap(uint256 toChainId, address fromERC20, address toERC20, uint128 amount) 
    external 
    override 
    addressCantBeZero(fromERC20)
    addressCantBeZero(toERC20)
    {
        require(_bridgableTokens[toChainId][fromERC20] != address(0), "Bridge: no swappable pair");
        address sender = msg.sender;

        IERC20MintableBurnable(fromERC20).burnFrom(sender ,amount);
        emit SwapInitialized(block.chainid, toChainId, fromERC20, toERC20, amount, sender);
    }

    function redeem(
        uint256 nonce,
        uint256 fromChainId,
        uint256 toChainId,
        address fromERC20,
        address toERC20,
        uint128 amount,
        address receiver,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external override {
        bytes32 message  = _getMessage(nonce, fromChainId, toChainId, fromERC20, toERC20, amount, receiver);

        require(!_redeemExecuted[message], "Bridge: redeem executed");
        
        bool valid = _checkSign(message ,v, r, s);

        require(valid, "Bridge: incorrect signer");
        _redeemExecuted[message] = true;

        IERC20MintableBurnable(toERC20).mint(receiver, amount);
    }

    function _checkSign(
        bytes32 message,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) private view returns (bool) {
        //bytes32 signedMsg = message.toEthSignedMessageHash();
        //address addr = signedMsg.recover(v, r, s);
        address addr = ecrecover(_hashMessage(message), v, r, s);

        if (addr == validator) {
            return true;
        }else {
            return false;
        }
    }

    function _hashMessage(bytes32 message) private pure returns(bytes32){
        bytes memory prefix = "\x19Ethereum Signed Message:\n32";
        return keccak256 (abi.encodePacked(prefix, message));
    }

    function _getMessage(
        uint256 nonce,
        uint256 fromChainId,
        uint256 toChainId,
        address fromERC20,
        address toERC20,
        uint128 amount,
        address receiver
    ) private pure returns (bytes32){
        bytes32 message = keccak256(
            abi.encodePacked(nonce, fromChainId, toChainId, fromERC20, toERC20, amount, receiver)
        );

        return message;
    }

}