// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

interface IBridge {
    
    event SwapInitialized(
        uint256 fromChainId,
        uint256 toChainId,
        address fromERC20,
        address toERC20,
        uint128 amount,
        address receiver
    );
    event ValidatorUpdated(address newValidator);
    event BridgableTokensUpdated(uint256 toChainId, address fromERC20, address toERC20);

    function checkSign(
        uint256 nonce,
        uint256 fromChainId,
        uint256 toChainId,
        address fromERC20,
        address toERC20,
        uint128 amount,
        address receiver,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external view returns (bool);

    function swap(uint256 toChainId, address fromERC20, address toERC20, uint128 amount) external;

    function redeem(
        uint256 nonce,
        uint256 fromChainId,
        uint256 toChainId,
        address fromERC20,
        address toERC20,
        uint128 amount,
        address receiver,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external;
}