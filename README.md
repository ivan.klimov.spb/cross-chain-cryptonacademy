#Do this first

Add your file .env to root project

Structure .env

```
PRIVATE_KEY = string
PRIVATE_KEY = string
PUBLIC_KEY = string
PRIVATE_KEY2 = string
PUBLIC_KEY2 = string
PRIVATE_KEY3 = string
PUBLIC_KEY3 = string
API_URL_RINKEBY = string
API_URL_GOERLI = string
API_KEY_ETHERSCAN = string
API_KEY_BSCSCAN = string
ERC20_FIRST = string
ERC20_SECOND = string
CHAIN_ID_FIRST = string
CHAIN_ID_SECOND = string
```

#Start

```
npm install
npx hardhat compile
```

#Run test

```
npx hardhat coverage
```

#Deploy contract to rinkeby via alchemy and bsc testnet

```
npx hardhat run scripts/deployERC20Token.ts --network rinkeby
npx hardhat run scripts/deployERC20Token.ts --network bsctestnet

npx hardhat run scripts/deployBridgeOnFirstChain.ts --network rinkeby
npx hardhat run scripts/deployBridgeOnSecondChain.ts --network bsctestnet
```

#Verify contract

```
npx hardhat verify --network rinkeby BRIDGE_ETH_ADDRESS VALIDATOR_ADDRESS OTHER_CHAIN_ID ERC20_ADDRESS OTHER_ERC20_ADDRESS  
npx hardhat verify --network bsctestnet BRIDGE_BSC_ADDRESS VALIDATOR_ADDRESS OTHER_CHAIN_ID ERC20_ADDRESS OTHER_ERC20_ADDRESS 
```

#Look at tasks list

```
npx hardhat  --help
```
