import dotenv from "dotenv";
dotenv.config({ path: __dirname + "/.env" });

import { ethers } from "hardhat";
import { ERC20BridgableToken } from "../src/types";

async function main() {
  const factory = await ethers.getContractFactory("ERC20BridgableToken");
  console.log("Deploying ERC20BridgableToken...");

  const contract: ERC20BridgableToken = await factory.deploy("IVKLIM", "KLIM");

  await contract.deployed();

  console.log("ERC20BridgableToken deployed to:", contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
