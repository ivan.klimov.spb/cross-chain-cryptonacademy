import dotenv from "dotenv";
dotenv.config({ path: __dirname + "/.env" });

const { PUBLIC_KEY, ERC20_FIRST, ERC20_SECOND, CHAIN_ID_SECOND } = process.env;

import { ethers } from "hardhat";
import { Bridge } from "../src/types";

async function main() {
  const factory = await ethers.getContractFactory("Bridge");
  console.log("Deploying Bridge...");

  const contract: Bridge = await factory.deploy(PUBLIC_KEY as string);

  await contract.deployed();

  console.log("Bridge deployed to:", contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
