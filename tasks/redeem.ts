import { task } from "hardhat/config";
import { TASK_REDEEM } from "./task-names";

task(TASK_REDEEM, "Redeen tokens on blockchein")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("nonce", "nonce")
  .addParam("fromChainId", "fromChainId")
  .addParam("toChainId", "toChainId")
  .addParam("fromERC20", "fromERC20")
  .addParam("toERC20", "toERC20")
  .addParam("amount", "amount")
  .addParam("receiver", "token receiver")
  .addParam("v", "v")
  .addParam("r", "r")
  .addParam("s", "s")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let bridge = await hre.ethers.getContractAt("IBridge", args.contract);

    await bridge
      .connect(account)
      .redeem(
        args.nonce,
        args.fromChainId,
        args.toChainId,
        args.fromERC20,
        args.toERC20,
        args.amount,
        args.receiver,
        args.v,
        args.r,
        args.s
      );

    console.log("task redeem finished");
  });
