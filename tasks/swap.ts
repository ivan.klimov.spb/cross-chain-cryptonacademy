import { task } from "hardhat/config";
import { TASK_SWAP } from "./task-names";

task(TASK_SWAP, "Swap tokens from blockchein to blockchein")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("toChainId", "toChainId")
  .addParam("fromERC20", "fromERC20")
  .addParam("toERC20", "toERC20")
  .addParam("amount", "amount")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let bridge = await hre.ethers.getContractAt("IBridge", args.contract);

    await bridge
      .connect(account)
      .swap(args.toChainId, args.fromERC20, args.toERC20, args.value);

    console.log("task swap finished");
  });
