import { task } from "hardhat/config";
import { TASK_GIVE_MINT_PERMISSION } from "./task-names";

task(TASK_GIVE_MINT_PERMISSION, "Give mint permission")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("minter", "address account that can mint")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let token = await hre.ethers.getContractAt(
      "ERC20BridgableToken",
      args.contract
    );

    const minterRole = await token.connect(account).MINTER_ROLE();

    await token.connect(account).grantRole(minterRole, args.minter);

    console.log("task give mint permission finished");
  });
