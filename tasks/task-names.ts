export const TASK_SWAP = "swap";
export const TASK_REDEEM = "redeem";
export const TASK_CHECK_SIGN = "checksign";
export const TASK_APPROVE = "approve";
export const TASK_GIVE_MINT_PERMISSION = "addMintPermission";
