/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */
import type {
  BaseContract,
  BigNumber,
  BigNumberish,
  BytesLike,
  CallOverrides,
  ContractTransaction,
  Overrides,
  PopulatedTransaction,
  Signer,
  utils,
} from "ethers";
import type {
  FunctionFragment,
  Result,
  EventFragment,
} from "@ethersproject/abi";
import type { Listener, Provider } from "@ethersproject/providers";
import type {
  TypedEventFilter,
  TypedEvent,
  TypedListener,
  OnEvent,
} from "../common";

export interface BridgeInterface extends utils.Interface {
  functions: {
    "checkSign(uint256,uint256,uint256,address,address,uint128,address,uint8,bytes32,bytes32)": FunctionFragment;
    "getBridgableTokens(uint256,address)": FunctionFragment;
    "redeem(uint256,uint256,uint256,address,address,uint128,address,uint8,bytes32,bytes32)": FunctionFragment;
    "setBridgableTokens(uint256,address,address)": FunctionFragment;
    "setValidator(address)": FunctionFragment;
    "swap(uint256,address,address,uint128)": FunctionFragment;
    "validator()": FunctionFragment;
  };

  getFunction(
    nameOrSignatureOrTopic:
      | "checkSign"
      | "getBridgableTokens"
      | "redeem"
      | "setBridgableTokens"
      | "setValidator"
      | "swap"
      | "validator"
  ): FunctionFragment;

  encodeFunctionData(
    functionFragment: "checkSign",
    values: [
      BigNumberish,
      BigNumberish,
      BigNumberish,
      string,
      string,
      BigNumberish,
      string,
      BigNumberish,
      BytesLike,
      BytesLike
    ]
  ): string;
  encodeFunctionData(
    functionFragment: "getBridgableTokens",
    values: [BigNumberish, string]
  ): string;
  encodeFunctionData(
    functionFragment: "redeem",
    values: [
      BigNumberish,
      BigNumberish,
      BigNumberish,
      string,
      string,
      BigNumberish,
      string,
      BigNumberish,
      BytesLike,
      BytesLike
    ]
  ): string;
  encodeFunctionData(
    functionFragment: "setBridgableTokens",
    values: [BigNumberish, string, string]
  ): string;
  encodeFunctionData(
    functionFragment: "setValidator",
    values: [string]
  ): string;
  encodeFunctionData(
    functionFragment: "swap",
    values: [BigNumberish, string, string, BigNumberish]
  ): string;
  encodeFunctionData(functionFragment: "validator", values?: undefined): string;

  decodeFunctionResult(functionFragment: "checkSign", data: BytesLike): Result;
  decodeFunctionResult(
    functionFragment: "getBridgableTokens",
    data: BytesLike
  ): Result;
  decodeFunctionResult(functionFragment: "redeem", data: BytesLike): Result;
  decodeFunctionResult(
    functionFragment: "setBridgableTokens",
    data: BytesLike
  ): Result;
  decodeFunctionResult(
    functionFragment: "setValidator",
    data: BytesLike
  ): Result;
  decodeFunctionResult(functionFragment: "swap", data: BytesLike): Result;
  decodeFunctionResult(functionFragment: "validator", data: BytesLike): Result;

  events: {
    "BridgableTokensUpdated(uint256,address,address)": EventFragment;
    "SwapInitialized(uint256,uint256,address,address,uint128,address)": EventFragment;
    "ValidatorUpdated(address)": EventFragment;
  };

  getEvent(nameOrSignatureOrTopic: "BridgableTokensUpdated"): EventFragment;
  getEvent(nameOrSignatureOrTopic: "SwapInitialized"): EventFragment;
  getEvent(nameOrSignatureOrTopic: "ValidatorUpdated"): EventFragment;
}

export interface BridgableTokensUpdatedEventObject {
  toChainId: BigNumber;
  fromERC20: string;
  toERC20: string;
}
export type BridgableTokensUpdatedEvent = TypedEvent<
  [BigNumber, string, string],
  BridgableTokensUpdatedEventObject
>;

export type BridgableTokensUpdatedEventFilter =
  TypedEventFilter<BridgableTokensUpdatedEvent>;

export interface SwapInitializedEventObject {
  fromChainId: BigNumber;
  toChainId: BigNumber;
  fromERC20: string;
  toERC20: string;
  amount: BigNumber;
  receiver: string;
}
export type SwapInitializedEvent = TypedEvent<
  [BigNumber, BigNumber, string, string, BigNumber, string],
  SwapInitializedEventObject
>;

export type SwapInitializedEventFilter = TypedEventFilter<SwapInitializedEvent>;

export interface ValidatorUpdatedEventObject {
  newValidator: string;
}
export type ValidatorUpdatedEvent = TypedEvent<
  [string],
  ValidatorUpdatedEventObject
>;

export type ValidatorUpdatedEventFilter =
  TypedEventFilter<ValidatorUpdatedEvent>;

export interface Bridge extends BaseContract {
  connect(signerOrProvider: Signer | Provider | string): this;
  attach(addressOrName: string): this;
  deployed(): Promise<this>;

  interface: BridgeInterface;

  queryFilter<TEvent extends TypedEvent>(
    event: TypedEventFilter<TEvent>,
    fromBlockOrBlockhash?: string | number | undefined,
    toBlock?: string | number | undefined
  ): Promise<Array<TEvent>>;

  listeners<TEvent extends TypedEvent>(
    eventFilter?: TypedEventFilter<TEvent>
  ): Array<TypedListener<TEvent>>;
  listeners(eventName?: string): Array<Listener>;
  removeAllListeners<TEvent extends TypedEvent>(
    eventFilter: TypedEventFilter<TEvent>
  ): this;
  removeAllListeners(eventName?: string): this;
  off: OnEvent<this>;
  on: OnEvent<this>;
  once: OnEvent<this>;
  removeListener: OnEvent<this>;

  functions: {
    checkSign(
      nonce: BigNumberish,
      fromChainId: BigNumberish,
      toChainId: BigNumberish,
      fromERC20: string,
      toERC20: string,
      amount: BigNumberish,
      receiver: string,
      v: BigNumberish,
      r: BytesLike,
      s: BytesLike,
      overrides?: CallOverrides
    ): Promise<[boolean]>;

    getBridgableTokens(
      toChainId: BigNumberish,
      fromERC20: string,
      overrides?: CallOverrides
    ): Promise<[string]>;

    redeem(
      nonce: BigNumberish,
      fromChainId: BigNumberish,
      toChainId: BigNumberish,
      fromERC20: string,
      toERC20: string,
      amount: BigNumberish,
      receiver: string,
      v: BigNumberish,
      r: BytesLike,
      s: BytesLike,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    setBridgableTokens(
      toChainId: BigNumberish,
      fromERC20: string,
      toERC20: string,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    setValidator(
      newValidator: string,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    swap(
      toChainId: BigNumberish,
      fromERC20: string,
      toERC20: string,
      amount: BigNumberish,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<ContractTransaction>;

    validator(overrides?: CallOverrides): Promise<[string]>;
  };

  checkSign(
    nonce: BigNumberish,
    fromChainId: BigNumberish,
    toChainId: BigNumberish,
    fromERC20: string,
    toERC20: string,
    amount: BigNumberish,
    receiver: string,
    v: BigNumberish,
    r: BytesLike,
    s: BytesLike,
    overrides?: CallOverrides
  ): Promise<boolean>;

  getBridgableTokens(
    toChainId: BigNumberish,
    fromERC20: string,
    overrides?: CallOverrides
  ): Promise<string>;

  redeem(
    nonce: BigNumberish,
    fromChainId: BigNumberish,
    toChainId: BigNumberish,
    fromERC20: string,
    toERC20: string,
    amount: BigNumberish,
    receiver: string,
    v: BigNumberish,
    r: BytesLike,
    s: BytesLike,
    overrides?: Overrides & { from?: string | Promise<string> }
  ): Promise<ContractTransaction>;

  setBridgableTokens(
    toChainId: BigNumberish,
    fromERC20: string,
    toERC20: string,
    overrides?: Overrides & { from?: string | Promise<string> }
  ): Promise<ContractTransaction>;

  setValidator(
    newValidator: string,
    overrides?: Overrides & { from?: string | Promise<string> }
  ): Promise<ContractTransaction>;

  swap(
    toChainId: BigNumberish,
    fromERC20: string,
    toERC20: string,
    amount: BigNumberish,
    overrides?: Overrides & { from?: string | Promise<string> }
  ): Promise<ContractTransaction>;

  validator(overrides?: CallOverrides): Promise<string>;

  callStatic: {
    checkSign(
      nonce: BigNumberish,
      fromChainId: BigNumberish,
      toChainId: BigNumberish,
      fromERC20: string,
      toERC20: string,
      amount: BigNumberish,
      receiver: string,
      v: BigNumberish,
      r: BytesLike,
      s: BytesLike,
      overrides?: CallOverrides
    ): Promise<boolean>;

    getBridgableTokens(
      toChainId: BigNumberish,
      fromERC20: string,
      overrides?: CallOverrides
    ): Promise<string>;

    redeem(
      nonce: BigNumberish,
      fromChainId: BigNumberish,
      toChainId: BigNumberish,
      fromERC20: string,
      toERC20: string,
      amount: BigNumberish,
      receiver: string,
      v: BigNumberish,
      r: BytesLike,
      s: BytesLike,
      overrides?: CallOverrides
    ): Promise<void>;

    setBridgableTokens(
      toChainId: BigNumberish,
      fromERC20: string,
      toERC20: string,
      overrides?: CallOverrides
    ): Promise<void>;

    setValidator(
      newValidator: string,
      overrides?: CallOverrides
    ): Promise<void>;

    swap(
      toChainId: BigNumberish,
      fromERC20: string,
      toERC20: string,
      amount: BigNumberish,
      overrides?: CallOverrides
    ): Promise<void>;

    validator(overrides?: CallOverrides): Promise<string>;
  };

  filters: {
    "BridgableTokensUpdated(uint256,address,address)"(
      toChainId?: null,
      fromERC20?: null,
      toERC20?: null
    ): BridgableTokensUpdatedEventFilter;
    BridgableTokensUpdated(
      toChainId?: null,
      fromERC20?: null,
      toERC20?: null
    ): BridgableTokensUpdatedEventFilter;

    "SwapInitialized(uint256,uint256,address,address,uint128,address)"(
      fromChainId?: null,
      toChainId?: null,
      fromERC20?: null,
      toERC20?: null,
      amount?: null,
      receiver?: null
    ): SwapInitializedEventFilter;
    SwapInitialized(
      fromChainId?: null,
      toChainId?: null,
      fromERC20?: null,
      toERC20?: null,
      amount?: null,
      receiver?: null
    ): SwapInitializedEventFilter;

    "ValidatorUpdated(address)"(
      newValidator?: null
    ): ValidatorUpdatedEventFilter;
    ValidatorUpdated(newValidator?: null): ValidatorUpdatedEventFilter;
  };

  estimateGas: {
    checkSign(
      nonce: BigNumberish,
      fromChainId: BigNumberish,
      toChainId: BigNumberish,
      fromERC20: string,
      toERC20: string,
      amount: BigNumberish,
      receiver: string,
      v: BigNumberish,
      r: BytesLike,
      s: BytesLike,
      overrides?: CallOverrides
    ): Promise<BigNumber>;

    getBridgableTokens(
      toChainId: BigNumberish,
      fromERC20: string,
      overrides?: CallOverrides
    ): Promise<BigNumber>;

    redeem(
      nonce: BigNumberish,
      fromChainId: BigNumberish,
      toChainId: BigNumberish,
      fromERC20: string,
      toERC20: string,
      amount: BigNumberish,
      receiver: string,
      v: BigNumberish,
      r: BytesLike,
      s: BytesLike,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<BigNumber>;

    setBridgableTokens(
      toChainId: BigNumberish,
      fromERC20: string,
      toERC20: string,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<BigNumber>;

    setValidator(
      newValidator: string,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<BigNumber>;

    swap(
      toChainId: BigNumberish,
      fromERC20: string,
      toERC20: string,
      amount: BigNumberish,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<BigNumber>;

    validator(overrides?: CallOverrides): Promise<BigNumber>;
  };

  populateTransaction: {
    checkSign(
      nonce: BigNumberish,
      fromChainId: BigNumberish,
      toChainId: BigNumberish,
      fromERC20: string,
      toERC20: string,
      amount: BigNumberish,
      receiver: string,
      v: BigNumberish,
      r: BytesLike,
      s: BytesLike,
      overrides?: CallOverrides
    ): Promise<PopulatedTransaction>;

    getBridgableTokens(
      toChainId: BigNumberish,
      fromERC20: string,
      overrides?: CallOverrides
    ): Promise<PopulatedTransaction>;

    redeem(
      nonce: BigNumberish,
      fromChainId: BigNumberish,
      toChainId: BigNumberish,
      fromERC20: string,
      toERC20: string,
      amount: BigNumberish,
      receiver: string,
      v: BigNumberish,
      r: BytesLike,
      s: BytesLike,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<PopulatedTransaction>;

    setBridgableTokens(
      toChainId: BigNumberish,
      fromERC20: string,
      toERC20: string,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<PopulatedTransaction>;

    setValidator(
      newValidator: string,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<PopulatedTransaction>;

    swap(
      toChainId: BigNumberish,
      fromERC20: string,
      toERC20: string,
      amount: BigNumberish,
      overrides?: Overrides & { from?: string | Promise<string> }
    ): Promise<PopulatedTransaction>;

    validator(overrides?: CallOverrides): Promise<PopulatedTransaction>;
  };
}
